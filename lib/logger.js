var tracer = require('tracer');
var fs = require('fs');
var config = require('./config');

(function(){
    if (!fs.existsSync(config.logPath)) {
        fs.mkdirSync(config.logPath);
    }
})();

module.exports = tracer.dailyfile({root: config.logPath});
