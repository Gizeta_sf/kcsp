var levelup = require('levelup');
var leveldown = require('leveldown');
var memCache = require('memory-cache');
var schedule = require('node-schedule');
var logger = require('./logger');
var config = require('./config');

var dbCache = levelup(config.cachePath);
var useMemCache = false;
var memCleaned = true;

/* start use memory-cache */
schedule.scheduleJob('59 3 * * *', function() {
    useMemCache = true;
    memCleaned = false;
});

/* clean leveldb */
schedule.scheduleJob('0 4 * * *', function() {
    dbCache.close();
    leveldown.destroy(config.cachePath, function(err) {
        logger.info('database has been cleaned.');
        
        dbCache = levelup(config.cachePath);
        useMemCache = false;
    });
});

/* clean memory-cache */
schedule.scheduleJob('1 4 * * *', function() {
    memCleaned = true;
    memCache.clear();
});

module.exports = {
    get: function(key, succFunc, notFoundFunc) {
        if (useMemCache) {
            var value = memCache.get(key);
            if (value != null) {
                succFunc(value);
                return;
            }
        }
        
        dbCache.get(key, function(err, value) {
            if (err) {
                if (err.notFound) {
                    if (notFoundFunc != null) {
                        if (!memCleaned) {
                            var value = memCache.get(key);
                            if (value != null) {
                                dbCache.put(key, value);
                                memCache.del(key);
                                succFunc(value);
                                return;
                            }
                        }
                        
                        notFoundFunc(key);
                    }
                    return;
                }
                
                logger.error('database error, %s', err);
                return;
            }
            
            if (succFunc != null) {
                if (useMemCache) {
                    memCache.put(key, value);
                }
                
                succFunc(value);
            }
        });
    },
    
    getSync: function(key, succFunc, notFoundFunc) {
        if (useMemCache) {
            var value = memCache.get(key);
            if (value != null) {
                succFunc(value);
                return;
            }
        }
        
        dbCache.get(key, {sync: true}, function(err, value) {
            if (err) {
                if (err.notFound) {
                    if (notFoundFunc != null) {
                        if (!memCleaned) {
                            var value = memCache.get(key);
                            if (value != null) {
                                dbCache.put(key, value);
                                memCache.del(key);
                                succFunc(value);
                                return;
                            }
                        }
                        
                        notFoundFunc(key);
                    }
                    return;
                }
                
                logger.error('database error, %s', err);
                return;
            }
            
            if (succFunc != null) {
                if (useMemCache) {
                    memCache.put(key, value);
                }
                
                succFunc(value);
            }
        });
    },
    
    put: function(key, value) {
        if (useMemCache) {
            memCache.put(key, value);
            return;
        }
        
        dbCache.put(key, value, function(err) {
            if (err) {
                logger.error('database error, %s', err);
            }
        });
    },
    
    putSync: function(key, value) {
        if (useMemCache) {
            memCache.put(key, value);
            return;
        }
        
        dbCache.put(key, value, {sync: true}, function(err) {
            if (err) {
                logger.error('database error, %s', err);
            }
        });
    },
    
    del: function(key) {
        if (useMemCache) {
            var value = memCache.get(key);
            if (value != null) {
                memCache.del(key);
                return;
            }
        }
        
        dbCache.del(key, function(err) {
            if (err) {
                logger.error('database error, %s', err);
            }
        });
    },
    
    clean: function() {
        dbCache.close();
        leveldown.destroy(config.cachePath, function(err) {
            logger.info('database has been cleaned.');
            
            dbCache = levelup(config.cachePath);
        });
    }
};
