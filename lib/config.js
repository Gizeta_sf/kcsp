var getopt = require('node-getopt');

var cmd = getopt.create([
    ['c', 'cache-path=ARG', 'set cache database path'],
    ['l', 'log-path=ARG', 'set log file path'],
    ['p', 'path=ARG', 'set base path']
]).bindHelp().parseSystem();

var basePath = cmd.options['path'] || '.';
var logPath = cmd.options['log-path'] || (basePath + '/log');
var cachePath = cmd.options['cache-path'] || (basePath + '/cache');

module.exports = {
    cachePath: cachePath,
    logPath: logPath,
    basePath: basePath
};
