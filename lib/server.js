var http = require('http');
var url = require('url');
var util = require('util');
var logger = require('./logger');
var uuid = require('node-uuid');
var request = require('request');
var db = require('./db');
var zlib = require('zlib');
var env = require('./env');

var RequestKind = {
    AuthFailed: -1,
    Invalid: 0,
    Api: 1,
    Resource: 2,
    CacheableApi: 3
};

var ResponseError = {
    403: '<h1>HTTP 403 - Forbidden</h1>参数错误。',
    404: '<h1>HTTP 404 - Not Found</h1>缓存未找到。',
    410: '<h1>HTTP 410 - Gone</h1>获取数据失败。',
    423: '<h1>HTTP 423 - Locked</h1>无法代理到指定地址。请检查地址是否输入错误。',
    500: '<h1>HTTP 500 - Internal Server Error</h1>服务器内部执行过程中遇到错误。请向webmaster提交错误报告以解决问题。',
    503: '<h1>HTTP 503 - Service Unavailable</h1>未获取到数据。请稍后再试。'
}

var kcDomainList = [
    '203.104.209.7',
    '203.104.209.71',
    '125.6.184.15',
    '125.6.184.16',
    '125.6.187.205',
    '125.6.187.229',
    '125.6.187.253',
    '125.6.188.25',
    '203.104.248.135',
    '125.6.189.7',
    '125.6.189.39',
    '125.6.189.71',
    '125.6.189.103',
    '125.6.189.135',
    '125.6.189.167',
    '125.6.189.215',
    '125.6.189.247',
    '203.104.209.23',
    '203.104.209.39',
    '203.104.209.55',
    '203.104.209.102',
    'moriyanojinjya.misaka.cn'
];
var kcCacheableApiList = [
    '/kcsapi/api_start2'
];

var server = http.createServer(function createCallback(req, resp) {
    var chunks = [];
    var chunkSize = 0;
    req.params = {
        ip: getIp(req),
        postData: '',
        remotePath: '',
        requestTime: new Date().getTime()
    };
    
    req.on('data', function(chunk) {
        chunks.push(chunk);
        chunkSize += chunk.length;
    });
    
    req.on('end', function() {
        var data = null;
        switch (chunks.length) {
            case 0:
                data = new Buffer(0);
                break;
            case 1:
                data = chunks[0];
                break;
            default:
                data = new Buffer(chunkSize);
                for (var i = 0, pos = 0, l = chunks.length; i < l; i++) {
                    var chunk = chunks[i];
                    chunk.copy(data, pos);
                    pos += chunk.length;
                }
                break;
        }
        req.params.postData = data.toString();
        
        logger.info('%s requests %s', req.params.ip, req.url);
        
        var urlObj = url.parse(req.url, true);
        if (urlObj.pathname === '/kcsp/get_cache') {
            if (urlObj.query.key) {
                db.get(urlObj.query.key, function(value) {
                    renderContent(resp, {code: 200, contentType: 'text/plain', content: value, request: req});
                }, function() {
                    renderErrorPage(resp, {code: 404, request: req});
                });
            }
            else {
                renderErrorPage(resp, {code: 403, request: req});
            }
        }
        else if (urlObj.pathname === '/kcsp/del_cache') {
            if (urlObj.query.key) {
                db.del(urlObj.query.key);
                renderContent(resp, {code: 200, contentType: 'text/plain', content: urlObj.query.key + ' has been deleted.', request: req});
            }
            else {
                renderErrorPage(resp, {code: 403, request: req});
            }
        }
        else if (urlObj.pathname === '/kcsp/destroy') {
            db.clean();
            renderContent(resp, {code: 200, contentType: 'text/plain', content: 'database has been destroyed.', request: req});
        }
        else if (urlObj.pathname === '/kcsp/lock') {
            db.put('lock', 'true');
            renderContent(resp, {code: 200, contentType: 'text/plain', content: 'kcsp has been locked.', request: req});
        }
        else if (urlObj.pathname === '/kcsp/unlock') {
            db.put('lock', 'false');
            renderContent(resp, {code: 200, contentType: 'text/plain', content: 'kcsp has been unlocked.', request: req});
        }
        else {
            db.getSync('lock', function(value) {
                if (value === 'true') {
                    renderErrorPage(resp, {code: 503, request: req});
                }
                else {
                    handleRequest(req, resp);
                }
            }, function() {
                handleRequest(req, resp);
            });
        }
    });
});

function handleRequest(req, resp) {
    switch (validateRequest(req)) {
        case RequestKind.AuthFailed:
            logger.error('auth failed.\n\tip: %s\n\turl: %s\n\tpost: %s\n\theader: %j', req.params.ip, req.url, req.params.postData, req.headers);
            
            renderErrorPage(resp, {code: 403, request: req});
            break;
        case RequestKind.Invalid:
            logger.error('illegal request.\n\tip: %s\n\turl: %s\n\tpost: %s\n\theader: %j', req.params.ip, req.url, req.params.postData, req.headers);
            
            renderErrorPage(resp, {code: 423, request: req});
            break;
        case RequestKind.Api:
            processApiRequest(req, resp);
            break;
        case RequestKind.Resource:
            processResourceRequest(req, resp);
            break;
        case RequestKind.CacheableApi:
            processCacheableApiRequest(req, resp);
            break;
    }
}

function validateUserToken(token) {
    if (token.indexOf("ProgitTest") >= 0) {
        db.put(token, 0);
        db.put('time_' + token, new Date().getTime());
        return true;
    }
    
    var v1 = parseInt(token.substr(1, 4), 16);
    var v2 = parseInt(token.substr(6, 4), 16);
    var v3 = parseInt(token.substr(9, 4), 16);
    
    var flag = false;
    if ((v1 ^ v2) === v3) {
        db.getSync(token, function(value) {
            if (parseInt(value) < 20) {
                flag = true;
            }
        }, function() {
            db.put(token, 0);
            db.put('time_' + token, new Date().getTime());
            flag = true;
        });
    }
    
    return flag;
}

function validateRequest(req) {
    if (req.headers['request-uri'] == null ||
        req.headers['cache-token'] == null ||
        req.headers['user-token'] == null)
        return RequestKind.Invalid;
    
    if (!validateUserToken(req.headers['user-token']))
        return RequestKind.AuthFailed;
    
    var objUrl = url.parse(req.headers['request-uri']);
    if (kcDomainList.indexOf(objUrl.hostname) >= 0) {
        var ext = objUrl.pathname.substr(-4);
        if (['.swf', '.png', '.mp3', '.js', '.css', '.jpg', '.gif', 'jpeg', 'html'].indexOf(ext) >= 0 || objUrl.hostname.indexOf('dmm.co') >= 0) {
            return RequestKind.Resource;
        }
        else if (kcCacheableApiList.indexOf(objUrl.pathname) >= 0) {
            req.params.remotePath = objUrl.pathname;
            return RequestKind.CacheableApi;
        }
        else {
            return RequestKind.Api;
        }
    }
    
    return RequestKind.Invalid;
}

function validateResponse(response) {
    try {
        var result = JSON.parse(response.substring(7));
        if (result.api_result === 1)
            return true;
        return false;
    } catch (ex) {
        return false;
    }
}

function processApiRequest(req, resp) {
    var cacheToken = req.headers['cache-token'];
    var userToken = req.headers['user-token'];
    
    logger.info('process request, user: %s, token: %s', userToken, cacheToken);
    
    db.get(cacheToken, function(value) {
        if (value === '__REQUEST__') {
            renderErrorPage(resp, {code: 503, request: req});
        }
        else if (value === '__BLOCK__') {
            renderErrorPage(resp, {code: 410, request: req });
        }
        else {
            renderContent(resp, {code: 200, contentType: 'text/plain', content: value, request: req});
        }
    }, function() {
        db.putSync(cacheToken, '__REQUEST__');
        
        postToRemote(resp, {
            url: req.headers['request-uri'],
            headers: filterHeaders(req.headers),
            postData: req.params.postData,
            userToken: userToken,
            cacheToken: cacheToken,
            request: req,
            ignoreError: true
        });
    });
}

function processResourceRequest(req, resp) {
    //todo
}

function processCacheableApiRequest(req, resp) {
    var cacheToken = req.params.remotePath;
    var userToken = req.headers['user-token'];
    
    logger.info('process request, user: %s, token: %s', userToken, cacheToken);
    
    db.get(cacheToken, function(value) {
        if (value === '__REQUEST__') {
            renderErrorPage(resp, {code: 503, request: req});
        }
        else {
            db.get('time_' + cacheToken, function(tokenValue) {
                if (parseInt(tokenValue) + 7200000 < new Date().getTime()) {
                    postToRemote(resp, {
                        url: req.headers['request-uri'],
                        headers: filterHeaders(req.headers),
                        postData: req.params.postData,
                        userToken: userToken,
                        cacheToken: cacheToken,
                        request: req,
                        ignoreError: true
                    });
                }
                else {
                    renderContent(resp, {code: 200, contentType: 'text/plain', content: value, request: req});
                    
                    request.post({
                        url: req.headers['request-uri'],
                        form: req.params.postData,
                        headers: filterHeaders(req.headers)
                    });
                }
            }, function() {
                postToRemote(resp, {
                    url: req.headers['request-uri'],
                    headers: filterHeaders(req.headers),
                    postData: req.params.postData,
                    userToken: userToken,
                    cacheToken: cacheToken,
                    request: req,
                    ignoreError: true,
                    validateResponse: true
                });
            });
        }
    }, function() {
        db.putSync(cacheToken, '__REQUEST__');
        
        postToRemote(resp, {
            url: req.headers['request-uri'],
            headers: filterHeaders(req.headers),
            postData: req.params.postData,
            userToken: userToken,
            cacheToken: cacheToken,
            request: req,
            ignoreError: true,
            validateResponse: true
        });
    });
}

function postToRemote(resp, data) {
    /* someone often sends bad url format*/
    var idx1 = data.url.indexOf(',http://') ;
    var idx2 = data.url.indexOf('?usr=');
    if (idx1 > 0 && idx2 > 0) {
        data.url = data.url.substring(0, Math.min(idx1, idx2));
    }
    else if (idx1 > 0 || idx2 > 0) {
        data.url = data.url.substring(0, Math.max(idx1, idx2));
    }
    
    db.put('time_' + data.cacheToken, new Date().getTime());
    
    logger.info('requesting %s', data.url);
    
    request.post({
        url: data.url,
        form: data.postData,
        headers: data.headers,
        timeout: 180000,
        gzip: true
    }, function (error, response, body) {
        if (!error) {
            if (response.statusCode >= 400 || (!!data.validateResponse && !validateResponse(body))) {
                logger.error('remote server response errors.\n\tcode: %d\n\turl: %s\n\tpost: %s\n\tresponse: %s', response.statusCode, data.url, data.postData, body);
                
                db.get(data.userToken, function(value) {
                    db.put(data.userToken, parseInt(value) + 1);
                });
                
                if (data.ignoreError) {
                    db.del(data.cacheToken);
                }
                else {
                    db.put(data.cacheToken, body);
                }
            }
            else {
                logger.info('remote server responsed, code: %d', response.statusCode);
                
                db.put(data.cacheToken, body);
            }
            renderContent(resp, {code: response.statusCode, contentType: 'text/plain', content: body, request: data.request});
        }
        else {
            logger.error('meet error during requesting.\n\terror: %s\n\turl: %s\n\tpost: %s', error, data.url, data.postData);
            
            if (data.ignoreError) {
                db.del(data.cacheToken);
            }
            else {
                db.put(data.cacheToken, '__BLOCK__');
            }
            
            renderErrorPage(resp, {code: 410, request: data.request});
        }
    });
}

function filterHeaders(data) {
    var headers = {};
    for (var key in data) {
        if (key !== 'host' && key !== 'content-length' && key !== 'cache-token' && key !== 'request-uri' && key !== 'user-token' && key !== 'retry-count' && key !== 'accept-encoding') {
            headers[key] = data[key];
        }
    }
    
    return headers;
}

function getIp(req) {
    return req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
}

function renderContent(resp, data, compress) {
    if (compress === false) {
        resp.writeHead(data.code, {'content-type': data.contentType});
        resp.end(data.content);
        
        logger.info('response to %s, handled in %ds', data.request.params.ip, (new Date().getTime() - data.request.params.requestTime) / 1000);
    }
    else {
        var acceptEncoding = data.request.headers['accept-encoding'] || '';
        if (acceptEncoding.indexOf('gzip') != -1) {
            zlib.gzip(data.content, function(err, result) {
                if (!err) {
                    resp.writeHead(data.code, {'content-type': data.contentType, 'content-encoding': 'gzip'});
                    resp.end(result);
                    
                    logger.info('response to %s, handled in %ds', data.request.params.ip, (new Date().getTime() - data.request.params.requestTime) / 1000);
                }
                else {
                    renderContent(resp, data, false);
                }
            });
        }
        else if (acceptEncoding.indexOf('deflate') != -1) {
            zlib.deflate(data.content, function(err, result) {
                if (!err) {
                    resp.writeHead(data.code, {'content-type': data.contentType, 'content-encoding': 'deflate'});
                    resp.end(result);
                    
                    logger.info('response to %s, handled in %ds', data.request.params.ip, (new Date().getTime() - data.request.params.requestTime) / 1000);
                }
                else {
                    renderContent(resp, data, false);
                }
            });
        }
        else {
            renderContent(resp, data, false);
        }
    }
}

function renderErrorPage(resp, data) {
    resp.writeHead(data.code, {'content-type': 'text/html'});
    resp.write('<!DOCTYPE html><html><head><meta charset="UTF-8"></head><body>');
    resp.write(ResponseError[data.code]);
    resp.end('<hr/>Powered by KCSP Server/' + env.APP_VERSION + '</body></html>');
    
    logger.info('send error to %s, handled in %ds', data.request.params.ip, (new Date().getTime() - data.request.params.requestTime) / 1000);
}

logger.info('kcsp server started.');

module.exports = server;
